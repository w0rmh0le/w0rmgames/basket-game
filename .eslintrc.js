module.exports = {
  root: true,
  env: {
    browser: true,
    commonjs: true
  },
  extends: [
    'eslint:recommended'
  ],
  rules: {
    'brace-style': ['error', 'stroustrup'],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'semi': ['error', 'always']
  },
  parserOptions: {
    ecmaVersion: 8,
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  overrides: [
    {
      files: ['**/*.spec.js'],
      env: {
        jest: true
      },
      plugins: ['jest'],
      rules: {
        'import/no-extraneous-dependencies': 'off',
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error'
      }
    }
  ]
}
