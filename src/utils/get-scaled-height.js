export default function (originalWidth, originalHeight, newWidth) {
  const scale = newWidth / originalWidth;
  return originalHeight * scale;
}
