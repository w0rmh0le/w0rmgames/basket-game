import getScaledHeight from '../get-scaled-height';

describe('getScaledHeight()', () => {
  it('should return the correct height for a given new width', () => {
    const originalWidth = 200;
    const originalHeight = 100;
    const newWidth = 100;
    const expectedHeight = 50;

    const newHeight = getScaledHeight(originalWidth, originalHeight, newWidth);

    expect(newHeight).toEqual(expectedHeight);
  });
});
