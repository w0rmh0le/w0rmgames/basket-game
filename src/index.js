import Phaser from 'phaser';

import getScaledHeight from './utils/get-scaled-height';


function preload () {
  // Backgrounds
  this.load.image('ol-comp', '../assets/png/ol-comp.png');

  // Middlegrounds
  this.load.image('cherry', '../assets/png/cherry_small.png');

  // Foregrounds
  this.load.image('basket', '../assets/png/basket_small.png');
}

function create () {
  // Background
  const olComp = this.add.image(0, 0, 'ol-comp').setOrigin(0, 0);
  const width = 800;
  olComp.displayWidth = width;
  olComp.displayHeight = 600;

  // Middleground
  const basket = this.physics.add.staticImage(400, 300, 'basket');

  const cherries = this.physics.add.group({
    key: 'cherry',
    repeat: 11,
    setXY: { x: 100, y: 50, stepX: 50 }
  });

  cherries.children.iterate(function (child) {
    child.setScale(0.25, 0.25);
    child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
    child.setCollideWorldBounds(true);
  });

  // Collision
  // this.physics.add.collider(basket, cherries);

  // Overlaps
  this.physics.add.overlap(basket, cherries, (basket, cherry) => {
    cherry.disableBody(true, true);
  }, null, this);
}

function update () {

}

const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 },
      debug: false
    }
  },
  scene: {
    preload: preload,
    create: create,
    update: update
  }
};

new Phaser.Game(config);
